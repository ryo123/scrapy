import os.path
from urllib.parse import urlparse
import arrow

from scrapy.http import HtmlResponse
from selenium import webdriver


driver = webdriver.PhantomJS()


class SeleniumDownloader(object):

    def process_request(self, request, spider):

        driver.get(request.url)


        return HtmlResponse(driver.current_url,
            body = driver.page_source,
            encoding = 'utf-8',
            request = request)

    def click(i):
    #     driver.find_element_by_link_text(i).click()
        driver.find_element_by_xpath("//*[@id="GridView1"]/tbody/tr[13]/td/table/tbody/tr/td[%d]/a"%i).click()

def close_driver():
    driver.close()
