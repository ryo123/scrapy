# -*- coding: utf-8 -*-
import scrapy
from gihyo.items import Kyujin
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from gihyo.downloader import SeleniumDownloader



class KyujinSpider(scrapy.Spider):
    name = 'kyujin'
    allowed_domains = ['kyuujin.hsc.ac.jp/torimatome.aspx']
    start_urls = ['http://kyuujin.hsc.ac.jp/torimatome.aspx']

    def parse(self, response):
        item = Kyujin()
        for i in range(2,12):
            for w in range (3,13):
                for data in response.css('tbody  tr:nth-child(%d)  td:nth-child(1) '%(w)):
                    print(data.css('a::attr("href")').extract_first())
                    # item['_id'] = data.css('a::text').extract_first()
                    # yield item
            if i ==11:
                break
            SeleniumDownloader.click(i)
