# -*- coding: utf-8 -*-
import scrapy
from gihyo.items import BookItem

class FeSpider(scrapy.Spider):
    name = 'fe'
    allowed_domains = ['gihyo.jp/book/genre?s=0802']
    start_urls = ['http://gihyo.jp/book/genre?s=0802']

    def parse(self, response):
        item = BookItem()
        for data in response.css('div .data'):
            item['title'] = data.css('h3 a').xpath('string()').extract_first()
            item['author'] = data.css('p.author::text').extract_first()
            item['price'] = data.css('p.price::text').extract_first()
            yield item
