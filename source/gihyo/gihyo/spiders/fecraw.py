# -*- coding: utf-8 -*-
import scrapy
from gihyo.items import BookItem
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class FecrawSpider(CrawlSpider):
    name = 'fecraw'
    allowed_domains = ['gihyo.jp']
    start_urls = ['http://gihyo.jp/book/genre?g=%E8%B3%87%E6%A0%BC%E8%A9%A6%E9%A8%93%EF%BC%88IT%EF%BC%89']
    rules = (
        Rule(LinkExtractor(allow=r'/book/genre\?s=0802$')),
        Rule(LinkExtractor(allow=r'/book/genre\?s=0802&page=[0-9]' ,deny=('t=sub')),callback='parse_books',follow=True),
    )

    def parse_books(self, response):
        item = BookItem()
        for data in response.css('div .data'):
            item['title'] = data.css('h3 a').xpath('string()').extract_first()
            item['author'] = data.css('p.author::text').extract_first()
            item['price'] = data.css('p.price::text').extract_first()
            yield item
