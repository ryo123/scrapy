import sys
import requests
import lxml.html
import re
from pymongo import MongoClient

def main():
    argvs = sys.argv
    if len(argvs) != 2:
        print("Usage: python3 gihyo.py URI")
        quit()
    else:
        URI = argvs[1]

    collection = dbconnect()
    scraping(collection,URI)

def scraping(collection,URI):
    r = requests.get(URI)
    root = lxml.html.fromstring(r.content)

    # URLを絶対パス(http://～)に変更
    root.make_links_absolute(r.url)

    # 本のタイトル/著者/税込み価格のスクレイピング
    for book in root.cssselect('div.data'):
        # シリーズ記載の有無
        if len(book[0]) >= 2:
            title = book[0][1].text_content() #シリーズが記載されている場合のタイトル位置
        else:
            title = book[0][0].text_content() #シリーズが記載されていない場合のタイトル位置

        # title=本のタイトル, book[1]=著者, book[3]=税抜き価格
        collection.insert_one({
            'title':title,
            'author':book[1].text_content().replace("　著",""),
            'price':round(int("".join(re.findall(r'[0-9]+',book[3].text_content())))*1.08)
        })

    # Nextリンクがあればクローリング
    for next_uri in root.cssselect('div.pageSwitch01.pageSwitch02.pageSwitchTop > p.next > a'):
        scraping(collection,next_uri.get('href'))


def dbconnect():
    client = MongoClient('localhost',27017)
    db = client.scraping
    return db.gihyo_fe

if __name__ == '__main__':
    main()
