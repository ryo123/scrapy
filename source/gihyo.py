import sys
import requests
import lxml.html
import re
from pymongo import MongoClient


def main():
    argvs = sys.argv
    if len(argvs) != 2:
        print("Usage: python3 gihyo.py URI")
        quit()
    else:
        URI=argvs[1]

    collection = dbconnect()
    scraping(collection,URI)


def scraping(collection):
    r = requests.get("http://gihyo.jp/book/genre?s=0804")
    root = lxml.html.fromstring(r.content)



    for div in root.cssselect('div.data'):
        print(div[0][1].text_content())
        t=div[1].text_content()
        t=t.replace('著','')
        

        s=div[3].text_content()
        s="".join(re.findall(r'[0-9]+',s))
        collection.insert_one({
            'title':div[0][1].text_content(),
            'author':t,
            'price':round(int(s)*1.08),
        })



def dbconnect():
    client=MongoClient('localhost',27017)
    db=client.scraping
    return db.gihyo_k


if __name__=='__main__':
    main()
