import sys
import requests
import lxml.html
import re
from pymongo import MongoClient
from selenium import webdriver
driver2=webdriver.PhantomJS()
driver=webdriver.PhantomJS()
def main():
    argvs = sys.argv
    if len(argvs) != 2:
        print("Usage: python3 kyujin.py URI")
        quit()
    else:
        URI = argvs[1]

    collection = dbconnect()
    scraping(collection,URI)

def scraping(collection,URI):

    driver.get(URI)
    driver.page_source.encode('utf-8')
    root = lxml.html.fromstring(driver.page_source)

    # URLを絶対パス(http://～)に変更
    root.make_links_absolute(driver.current_url)

    for i in range (2,12):
        root = lxml.html.fromstring(driver.page_source)
        for w in range (3,13):
            for job in root.cssselect('tbody > tr:nth-child(%d) > td:nth-child(1)>a'%(w)):
                root.make_links_absolute(driver.current_url)
                # print(job.get('href'))
                URI2=job.get('href')
                scraping2(collection,URI2)
        if i ==11:
            break
        driver.find_element_by_link_text(i).click()
                # collection.insert_one(
                #     #'_id':,
                #     'title':title,
                #     'author':book[1].text_content().replace("　著",""),
                #     'price':round(int("".join(re.findall(r'[0-9]+',book[3].text_content())))*1.08)
                # })

def scraping2(collection,URI2):
    # driver=webdriver.PhantomJS()
    driver2.get(URI2)
    driver2.page_source.encode('utf-8')
    root = lxml.html.fromstring(driver2.page_source)

    # for x in range(0,5):
    for job2 in root.cssselect('tbody'):#main > table > tbody
            # if x==1:
            #     print(job2[x][1].text_content().strip())
            #     print(job2[x][3].text_content().strip())
            #     print(job2[x][5].text_content().strip())
            # test=job2[x][1].text_content().strip()
            # print(test)
    # print("")
        collection.insert_one({
            '_id':job2[1][1].text_content().strip(),
            'year':job2[0][1].text_content().strip(),
            'matome':job2[1][3].text_content().strip(),
            'uketsuke':job2[1][5].text_content().strip(),
            'huri':job2[2][1].text_content().strip(),
            'name':job2[3][1].text_content().strip(),
            'url':job2[4][1].text_content().strip()
            })


def dbconnect():
    client = MongoClient('localhost',27017)
    db = client.scraping
    return db.kyuujin

if __name__ == '__main__':
    main()
